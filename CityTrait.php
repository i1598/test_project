<?php

trait CityTrait{
    /**
     *获取所有城市
     */
    public static function getAllCities(){
		$list = array(
			self::CITY_ID_BEIJING => array('id' => '110000','name' => '北京','host' => 'bj.lianjia.com','short' => 'bj','province' => '北京',),
			self::CITY_ID_SHANGHAI => array('id' => '310000','name' => '上海','host' => 'sh.lianjia.com','short' => 'sh','province' => '上海',),
			self::CITY_ID_GUANGZHOU => array('id' => '440100','name' => '广州','host' => 'gz.lianjia.com','short' => 'gz','province' => '广东',),
			self::CITY_ID_SHENZHEN => array('id' => '440300','name' => '深圳','host' => 'sz.lianjia.com','short' => 'sz','province' => '广东',),
			self::CITY_ID_TIANJIN => array('id' => '120000','name' => '天津','host' => 'tj.lianjia.com','short' => 'tj','province' => '天津',),
			self::CITY_ID_CHENGDU => array('id' => '510100','name' => '成都','host' => 'cd.lianjia.com','short' => 'cd','province' => '四川',),
			self::CITY_ID_NANJING => array('id' => '320100','name' => '南京','host' => 'nj.lianjia.com','short' => 'nj','province' => '江苏',),
			self::CITY_ID_HANGZHOU => array('id' => '330100','name' => '杭州','host' => 'hz.lianjia.com','short' => 'hz','province' => '浙江',),
			self::CITY_ID_QINGDAO => array('id' => '370200','name' => '青岛','host' => 'qd.lianjia.com','short' => 'qd','province' => '山东',),
			self::CITY_ID_DALIAN => array('id' => '210200','name' => '大连','host' => 'dl.lianjia.com','short' => 'dl','province' => '辽宁',),
			self::CITY_ID_SUZHOU => array('id' => '320500','name' => '苏州','host' => 'su.lianjia.com','short' => 'su','province' => '江苏',),
			self::CITY_ID_XIAMEN => array('id' => '350200','name' => '厦门','host' => 'xm.lianjia.com','short' => 'xm','province' => '福建',),
			self::CITY_ID_WUHAN => array('id' => '420100','name' => '武汉','host' => 'wh.lianjia.com','short' => 'wh','province' => '湖北',),
			self::CITY_ID_CHONGQING => array('id' => '500000','name' => '重庆','host' => 'cq.lianjia.com','short' => 'cq','province' => '重庆',),
			self::CITY_ID_CHANGSHA => array('id' => '430100','name' => '长沙','host' => 'cs.lianjia.com','short' => 'cs','province' => '湖南',),
			self::CITY_ID_JINAN => array('id' => '370101','name' => '济南','host' => 'jn.lianjia.com','short' => 'jn','province' => '山东',),
			self::CITY_ID_FOSHAN => array('id' => '440600','name' => '佛山','host' => 'fs.lianjia.com','short' => 'fs','province' => '广东',),
			self::CITY_ID_DONGGUAN => array('id' => '441900','name' => '东莞','host' => 'dg.lianjia.com','short' => 'dg','province' => '广东',),
			self::CITY_ID_XUNICHENGSHI => array('id' => '888888','name' => '测试城市','host' => 'vi.lianjia.com','short' => 'vi','province' => '无',),
			self::CITY_ID_XIAN => array('id' => '610100','name' => '西安','host' => 'xa.lianjia.com','short' => 'xa','province' => '陕西',),
			self::CITY_ID_SHIJIAZHUANG => array('id' => '130100','name' => '石家庄','host' => 'sjz.lianjia.com','short' => 'sjz','province' => '河北',),
			self::CITY_ID_YANTAI => array('id' => '370600','name' => '烟台','host' => 'yt.lianjia.com','short' => 'yt','province' => '山东',),
			self::CITY_ID_WENZHOU => array('id' => '330300','name' => '温州','host' => 'wz.lianjia.com','short' => 'wz','province' => '浙江',),
			self::CITY_ID_ZHONGSHAN => array('id' => '442000','name' => '中山','host' => 'zs.lianjia.com','short' => 'zs','province' => '广东',),
			self::CITY_ID_TANGSHAN => array('id' => '130200','name' => '唐山','host' => 'ts.lianjia.com','short' => 'ts','province' => '河北',),
			self::CITY_ID_ZHUHAI => array('id' => '440400','name' => '珠海','host' => 'zh.lianjia.com','short' => 'zh','province' => '广东',),
			self::CITY_ID_JIAXING => array('id' => '330400','name' => '嘉兴','host' => 'jx.lianjia.com','short' => 'jx','province' => '江苏',),
			self::CITY_ID_HUIZHOU => array('id' => '441300','name' => '惠州','host' => 'hui.lianjia.com','short' => 'hui','province' => '广东',),
			self::CITY_ID_YANGZHOU => array('id' => '321000','name' => '扬州','host' => 'yz.lianjia.com','short' => 'yz','province' => '江苏',),
			self::CITY_ID_TAIYUAN => array('id' => '140100','name' => '太原','host' => 'ty.lianjia.com','short' => 'ty','province' => '山西',),
			self::CITY_ID_SHENYANG => array('id' => '210100','name' => '沈阳','host' => 'sy.lianjia.com','short' => 'sy','province' => '辽宁',),
			self::CITY_ID_NANTONG => array('id' => '320600','name' => '南通','host' => 'nt.lianjia.com','short' => 'nt','province' => '江苏',),
			self::CITY_ID_HAIKOU => array('id' => '460100','name' => '海口','host' => 'hk.lianjia.com','short' => 'hk','province' => '海南',),
			self::CITY_ID_WEIFANG => array('id' => '370700','name' => '潍坊','host' => 'wf.lianjia.com','short' => 'wf','province' => '山东',),
			self::CITY_ID_LINYI => array('id' => '371300','name' => '临沂','host' => 'lin.lianjia.com','short' => 'lin','province' => '山东',),
			self::CITY_ID_WUXI => array('id' => '320200','name' => '无锡','host' => 'wx.lianjia.com','short' => 'wx','province' => '江苏',),
			self::CITY_ID_XUZHOU => array('id' => '320300','name' => '徐州','host' => 'xz.lianjia.com','short' => 'xz','province' => '江苏',),
			self::CITY_ID_SANYA => array('id' => '460200','name' => '三亚','host' => 'san.lianjia.com','short' => 'san','province' => '海南',),
			self::CITY_ID_WENCHANG => array('id' => '469005','name' => '文昌','host' => 'wc.lianjia.com','short' => 'wc','province' => '海南',),
			self::CITY_ID_QIONGHAI => array('id' => '469002','name' => '琼海','host' => 'qh.lianjia.com','short' => 'qh','province' => '海南',),
			self::CITY_ID_LINGSHUI => array('id' => '469028','name' => '陵水','host' => 'ls.lianjia.com','short' => 'ls','province' => '海南',),
			self::CITY_ID_WANNING => array('id' => '469006','name' => '万宁','host' => 'wn.lianjia.com','short' => 'wn','province' => '海南',),
			self::CITY_ID_LANGFANG => array('id' => '131000','name' => '廊坊','host' => 'bj.lianjia.com','short' => 'bj','province' => '河北',),
			self::CITY_ID_HEFEI => array('id' => '340100','name' => '合肥','host' => 'hf.lianjia.com','short' => 'hf','province' => '安徽',),
		);
		return $list;
    }

    /**
     *m站获取所有城市
     */
    public static function getAllCitiesForMobile(){
    		$list = array(
			self::CITY_ID_BEIJING => array('id' => '110000','name' => '北京','host' => 'm.lianjia.com/bj','short' => 'bj','province' => '北京','longitude' => '116.4138','latitude' => '39.8992',),
			self::CITY_ID_SHANGHAI => array('id' => '310000','name' => '上海','host' => 'm.lianjia.com/sh','short' => 'sh','province' => '上海','longitude' => '121.4804','latitude' => '31.2368',),
			self::CITY_ID_GUANGZHOU => array('id' => '440100','name' => '广州','host' => 'm.lianjia.com/gz','short' => 'gz','province' => '广东','longitude' => '113.27373','latitude' => '23.128823',),
			self::CITY_ID_SHENZHEN => array('id' => '440300','name' => '深圳','host' => 'm.lianjia.com/sz','short' => 'sz','province' => '广东','longitude' => '114.051845','latitude' => '22.556468',),
			self::CITY_ID_TIANJIN => array('id' => '120000','name' => '天津','host' => 'm.lianjia.com/tj','short' => 'tj','province' => '天津','longitude' => '117.204208','latitude' => '39.096496',),
			self::CITY_ID_CHENGDU => array('id' => '510100','name' => '成都','host' => 'm.lianjia.com/cd','short' => 'cd','province' => '四川','longitude' => '104.06','latitude' => '30.67',),
			self::CITY_ID_NANJING => array('id' => '320100','name' => '南京','host' => 'm.lianjia.com/nj','short' => 'nj','province' => '江苏','longitude' => '118.802948','latitude' => '32.122127',),
			self::CITY_ID_HANGZHOU => array('id' => '330100','name' => '杭州','host' => 'm.lianjia.com/hz','short' => 'hz','province' => '浙江','longitude' => '120.1601','latitude' => '30.2802',),
			self::CITY_ID_QINGDAO => array('id' => '370200','name' => '青岛','host' => 'm.lianjia.com/qd','short' => 'qd','province' => '山东','longitude' => '120.33','latitude' => '36.07',),
			self::CITY_ID_DALIAN => array('id' => '210200','name' => '大连','host' => 'm.lianjia.com/dl','short' => 'dl','province' => '辽宁','longitude' => '121.597023','latitude' => '38.91401',),
			self::CITY_ID_SUZHOU => array('id' => '320500','name' => '苏州','host' => 'm.lianjia.com/su','short' => 'su','province' => '江苏','longitude' => '120.5811424828','latitude' => '31.3010678543',),
			self::CITY_ID_XIAMEN => array('id' => '350200','name' => '厦门','host' => 'm.lianjia.com/xm','short' => 'xm','province' => '福建','longitude' => '118.092388','latitude' => '24.488705',),
			self::CITY_ID_WUHAN => array('id' => '420100','name' => '武汉','host' => 'm.lianjia.com/wh','short' => 'wh','province' => '湖北','longitude' => '114.307576','latitude' => '30.597996',),
			self::CITY_ID_CHONGQING => array('id' => '500000','name' => '重庆','host' => 'm.lianjia.com/cq','short' => 'cq','province' => '重庆','longitude' => '106.56168','latitude' => '29.573258',),
			self::CITY_ID_CHANGSHA => array('id' => '430100','name' => '长沙','host' => 'm.lianjia.com/cs','short' => 'cs','province' => '湖南','longitude' => '112.939684','latitude' => '28.234102',),
			self::CITY_ID_JINAN => array('id' => '370101','name' => '济南','host' => 'm.lianjia.com/jn','short' => 'jn','province' => '山东','longitude' => '116.988747','latitude' => '36.672828',),
			self::CITY_ID_FOSHAN => array('id' => '440600','name' => '佛山','host' => 'm.lianjia.com/fs','short' => 'fs','province' => '广东','longitude' => '113.134026','latitude' => '23.035095',),
			self::CITY_ID_DONGGUAN => array('id' => '441900','name' => '东莞','host' => 'm.lianjia.com/dg','short' => 'dg','province' => '广东','longitude' => '113.752482','latitude' => '23.003846',),
			self::CITY_ID_XUNICHENGSHI => array('id' => '888888','name' => '测试城市','host' => 'm.lianjia.com/vi','short' => 'vi','province' => '无','longitude' => '116.4138','latitude' => '39.8992',),
			self::CITY_ID_XIAN => array('id' => '610100','name' => '西安','host' => 'm.lianjia.com/xa','short' => 'xa','province' => '陕西','longitude' => '108.949074','latitude' => '34.26539',),
			self::CITY_ID_SHIJIAZHUANG => array('id' => '130100','name' => '石家庄','host' => 'm.lianjia.com/sjz','short' => 'sjz','province' => '河北','longitude' => '114.518633','latitude' => '38.048731',),
			self::CITY_ID_YANTAI => array('id' => '370600','name' => '烟台','host' => 'm.lianjia.com/yt','short' => 'yt','province' => '山东','longitude' => '121.451904','latitude' => '37.469765',),
			self::CITY_ID_WENZHOU => array('id' => '330300','name' => '温州','host' => 'm.lianjia.com/wz','short' => 'wz','province' => '浙江','longitude' => '120.65','latitude' => '28.01',),
			self::CITY_ID_ZHONGSHAN => array('id' => '442000','name' => '中山','host' => 'm.lianjia.com/zs','short' => 'zs','province' => '广东','longitude' => '113.42206','latitude' => '22.545178',),
			self::CITY_ID_TANGSHAN => array('id' => '130200','name' => '唐山','host' => 'm.lianjia.com/ts','short' => 'ts','province' => '河北','longitude' => '118.02','latitude' => '39.63',),
			self::CITY_ID_ZHUHAI => array('id' => '440400','name' => '珠海','host' => 'm.lianjia.com/zh','short' => 'zh','province' => '广东','longitude' => '113.562447','latitude' => '22.256915',),
			self::CITY_ID_JIAXING => array('id' => '330400','name' => '嘉兴','host' => 'm.lianjia.com/jx','short' => 'jx','province' => '江苏','longitude' => '120.76','latitude' => '30.77',),
			self::CITY_ID_HUIZHOU => array('id' => '441300','name' => '惠州','host' => 'm.lianjia.com/hui','short' => 'hui','province' => '广东','longitude' => '114.422691','latitude' => '23.118805',),
			self::CITY_ID_YANGZHOU => array('id' => '321000','name' => '扬州','host' => 'm.lianjia.com/yz','short' => 'yz','province' => '江苏','longitude' => '119.42','latitude' => '32.39',),
			self::CITY_ID_TAIYUAN => array('id' => '140100','name' => '太原','host' => 'm.lianjia.com/ty','short' => 'ty','province' => '山西','longitude' => '112.53','latitude' => '37.87',),
			self::CITY_ID_SHENYANG => array('id' => '210100','name' => '沈阳','host' => 'm.lianjia.com/sy','short' => 'sy','province' => '辽宁','longitude' => '123.38','latitude' => '41.8',),
			self::CITY_ID_NANTONG => array('id' => '320600','name' => '南通','host' => 'm.lianjia.com/nt','short' => 'nt','province' => '江苏','longitude' => '120.86','latitude' => '32.01',),
			self::CITY_ID_HAIKOU => array('id' => '460100','name' => '海口','host' => 'm.lianjia.com/hk','short' => 'hk','province' => '海南','longitude' => '110.330802','latitude' => '20.022071',),
			self::CITY_ID_WEIFANG => array('id' => '370700','name' => '潍坊','host' => 'm.lianjia.com/wf','short' => 'wf','province' => '山东','longitude' => '119.1','latitude' => '36.62',),
			self::CITY_ID_LINYI => array('id' => '371300','name' => '临沂','host' => 'm.lianjia.com/lin','short' => 'lin','province' => '山东','longitude' => '118.35','latitude' => '35.05',),
			self::CITY_ID_WUXI => array('id' => '320200','name' => '无锡','host' => 'm.lianjia.com/wx','short' => 'wx','province' => '江苏','longitude' => '120.31293','latitude' => '31.497401',),
			self::CITY_ID_XUZHOU => array('id' => '320300','name' => '徐州','host' => 'm.lianjia.com/xz','short' => 'xz','province' => '江苏','longitude' => '117.2','latitude' => '34.26',),
			self::CITY_ID_SANYA => array('id' => '460200','name' => '三亚','host' => 'm.lianjia.com/san','short' => 'san','province' => '海南','longitude' => '109.50','latitude' => '18.25',),
			self::CITY_ID_WENCHANG => array('id' => '469005','name' => '文昌','host' => 'm.lianjia.com/wc','short' => 'wc','province' => '海南','longitude' => '109.50','latitude' => '18.25',),
			self::CITY_ID_QIONGHAI => array('id' => '469002','name' => '琼海','host' => 'm.lianjia.com/qh','short' => 'qh','province' => '海南','longitude' => '109.50','latitude' => '18.25',),
			self::CITY_ID_LINGSHUI => array('id' => '469028','name' => '陵水','host' => 'm.lianjia.com/ls','short' => 'ls','province' => '海南','longitude' => '109.50','latitude' => '18.5123869278',),
			self::CITY_ID_WANNING => array('id' => '469006','name' => '万宁','host' => 'm.lianjia.com/wn','short' => 'wn','province' => '海南','longitude' => '109.50','latitude' => '18.25',),
			self::CITY_ID_LANGFANG => array('id' => '131000','name' => '廊坊','host' => '','short' => 'bj','province' => '河北','longitude' => '0','latitude' => '0',),
			self::CITY_ID_HEFEI => array('id' => '340100','name' => '合肥','host' => 'm.lianjia.com/hf','short' => 'hf','province' => '安徽','longitude' => '','latitude' => '',),
		);
		return $list;
    }

    /**
     *获取省和市的组合
     */
    public static function hasProvinceCity($provinceName, $cityName){
		if($provinceName == '北京' && $cityName == '北京') {return self::CITY_ID_BEIJING;}
		if($provinceName == '上海' && $cityName == '上海') {return self::CITY_ID_SHANGHAI;}
		if($provinceName == '广东' && $cityName == '广州') {return self::CITY_ID_GUANGZHOU;}
		if($provinceName == '广东' && $cityName == '深圳') {return self::CITY_ID_SHENZHEN;}
		if($provinceName == '天津' && $cityName == '天津') {return self::CITY_ID_TIANJIN;}
		if($provinceName == '四川' && $cityName == '成都') {return self::CITY_ID_CHENGDU;}
		if($provinceName == '江苏' && $cityName == '南京') {return self::CITY_ID_NANJING;}
		if($provinceName == '浙江' && $cityName == '杭州') {return self::CITY_ID_HANGZHOU;}
		if($provinceName == '山东' && $cityName == '青岛') {return self::CITY_ID_QINGDAO;}
		if($provinceName == '辽宁' && $cityName == '大连') {return self::CITY_ID_DALIAN;}
		if($provinceName == '江苏' && $cityName == '苏州') {return self::CITY_ID_SUZHOU;}
		if($provinceName == '福建' && $cityName == '厦门') {return self::CITY_ID_XIAMEN;}
		if($provinceName == '湖北' && $cityName == '武汉') {return self::CITY_ID_WUHAN;}
		if($provinceName == '重庆' && $cityName == '重庆') {return self::CITY_ID_CHONGQING;}
		if($provinceName == '湖南' && $cityName == '长沙') {return self::CITY_ID_CHANGSHA;}
		if($provinceName == '山东' && $cityName == '济南') {return self::CITY_ID_JINAN;}
		if($provinceName == '广东' && $cityName == '佛山') {return self::CITY_ID_FOSHAN;}
		if($provinceName == '广东' && $cityName == '东莞') {return self::CITY_ID_DONGGUAN;}
		if($provinceName == '无' && $cityName == '测试城市') {return self::CITY_ID_XUNICHENGSHI;}
		if($provinceName == '陕西' && $cityName == '西安') {return self::CITY_ID_XIAN;}
		if($provinceName == '河北' && $cityName == '石家庄') {return self::CITY_ID_SHIJIAZHUANG;}
		if($provinceName == '山东' && $cityName == '烟台') {return self::CITY_ID_YANTAI;}
		if($provinceName == '浙江' && $cityName == '温州') {return self::CITY_ID_WENZHOU;}
		if($provinceName == '广东' && $cityName == '中山') {return self::CITY_ID_ZHONGSHAN;}
		if($provinceName == '河北' && $cityName == '唐山') {return self::CITY_ID_TANGSHAN;}
		if($provinceName == '广东' && $cityName == '珠海') {return self::CITY_ID_ZHUHAI;}
		if($provinceName == '江苏' && $cityName == '嘉兴') {return self::CITY_ID_JIAXING;}
		if($provinceName == '广东' && $cityName == '惠州') {return self::CITY_ID_HUIZHOU;}
		if($provinceName == '江苏' && $cityName == '扬州') {return self::CITY_ID_YANGZHOU;}
		if($provinceName == '山西' && $cityName == '太原') {return self::CITY_ID_TAIYUAN;}
		if($provinceName == '辽宁' && $cityName == '沈阳') {return self::CITY_ID_SHENYANG;}
		if($provinceName == '江苏' && $cityName == '南通') {return self::CITY_ID_NANTONG;}
		if($provinceName == '海南' && $cityName == '海口') {return self::CITY_ID_HAIKOU;}
		if($provinceName == '山东' && $cityName == '潍坊') {return self::CITY_ID_WEIFANG;}
		if($provinceName == '山东' && $cityName == '临沂') {return self::CITY_ID_LINYI;}
		if($provinceName == '江苏' && $cityName == '无锡') {return self::CITY_ID_WUXI;}
		if($provinceName == '江苏' && $cityName == '徐州') {return self::CITY_ID_XUZHOU;}
		if($provinceName == '海南' && $cityName == '三亚') {return self::CITY_ID_SANYA;}
		if($provinceName == '海南' && $cityName == '文昌') {return self::CITY_ID_WENCHANG;}
		if($provinceName == '海南' && $cityName == '琼海') {return self::CITY_ID_QIONGHAI;}
		if($provinceName == '海南' && $cityName == '陵水') {return self::CITY_ID_LINGSHUI;}
		if($provinceName == '海南' && $cityName == '万宁') {return self::CITY_ID_WANNING;}
		if($provinceName == '河北' && $cityName == '廊坊') {return self::CITY_ID_LANGFANG;}
		if($provinceName == '安徽' && $cityName == '合肥') {return self::CITY_ID_HEFEI;}
		return false;
    }

    /**
     *获取旅居城市id列表
     */
    public static function getLvjuCityIds(){
		$list = array(
			self::CITY_ID_HAIKOU,
			self::CITY_ID_SANYA,
			self::CITY_ID_WENCHANG,
			self::CITY_ID_QIONGHAI,
			self::CITY_ID_LINGSHUI,
			self::CITY_ID_WANNING,
		);
		return $list;
    }

    /**
    *获取百城城市id列表
    */
    public static function getBaichengCityIds(){
		$list = array(
			self::CITY_ID_GUANGZHOU,
			self::CITY_ID_XIAMEN,
			self::CITY_ID_FOSHAN,
			self::CITY_ID_DONGGUAN,
			self::CITY_ID_YANTAI,
			self::CITY_ID_WENZHOU,
			self::CITY_ID_ZHONGSHAN,
			self::CITY_ID_TANGSHAN,
			self::CITY_ID_ZHUHAI,
			self::CITY_ID_JIAXING,
			self::CITY_ID_HUIZHOU,
			self::CITY_ID_YANGZHOU,
			self::CITY_ID_TAIYUAN,
			self::CITY_ID_SHENYANG,
			self::CITY_ID_NANTONG,
			self::CITY_ID_HAIKOU,
			self::CITY_ID_WEIFANG,
			self::CITY_ID_LINYI,
			self::CITY_ID_WUXI,
			self::CITY_ID_XUZHOU,
			self::CITY_ID_SANYA,
			self::CITY_ID_WENCHANG,
			self::CITY_ID_QIONGHAI,
			self::CITY_ID_LINGSHUI,
			self::CITY_ID_WANNING,
		);
		return $list;
    }
}