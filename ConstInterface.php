<?php

interface ConstInterface{
    /**
     *定义所有服务
     */
	const CONST_TYPE_PRICE = 'sell_price_set';
	const CONST_TYPE_RENT = 'rent_price_set';
	const CONST_TYPE_AREA = 'house_area_set';
	const CONST_TYPE_RENT_AREA = 'rent_area_set';
	const CONST_TYPE_ROOM = 'house_room_set';
	const CONST_TYPE_TAG = 'house_tag';
	const CONST_TYPE_SOURCE = 'source_appid';
	const CONST_TYPE_FRAMESTRUCKTURE = 'frame_structure';
	const CONST_TYPE_RESBLOCK_PRICE = 'resblock_price';
}