
<?php

trait ConstTrait{
    /**
     *校验是否有服务
     */
    public static function getCityConst($cityId, $constType){
		if($constType == self::CONST_TYPE_PRICE) {
			switch($cityId) {
				case self::CITY_ID_BEIJING:
					$option = array(200,250,300,400,500,800,1000);
					break;
				case self::CITY_ID_SHANGHAI:
					$option = array(100,150,200,300,500,800,1000);
					break;
				case self::CITY_ID_GUANGZHOU:
					$option = array(100,120,150,200,300,500);
					break;
				case self::CITY_ID_SHENZHEN:
					$option = array(200,300,400,500,800,1000);
					break;
				case self::CITY_ID_TIANJIN:
					$option = array(80,100,150,200,300,500);
					break;
				case self::CITY_ID_CHENGDU:
					$option = array(40,60,80,100,150,200,300);
					break;
				case self::CITY_ID_NANJING:
					$option = array(80,100,150,200,300,500);
					break;
				case self::CITY_ID_HANGZHOU:
					$option = array(100,150,200,300,500);
					break;
				case self::CITY_ID_QINGDAO:
					$option = array(80,100,150,200,300,500);
					break;
				case self::CITY_ID_DALIAN:
					$option = array(40,60,80,100,150,200);
					break;
				case self::CITY_ID_XIAMEN:
					$option = array(100,200,300,400,500,800);
					break;
				case self::CITY_ID_WUHAN:
					$option = array(80,100,150,200,300);
					break;
				case self::CITY_ID_CHONGQING:
					$option = array(40,60,80,100,150,200);
					break;
				case self::CITY_ID_CHANGSHA:
					$option = array(40,60,80,100,150);
					break;
				case self::CITY_ID_JINAN:
					$option = array(50,80,100,150,200);
					break;
				case self::CITY_ID_FOSHAN:
					$option = array(50,80,100,150,200,300,500);
					break;
				case self::CITY_ID_DONGGUAN:
					$option = array(50,80,100,150,200,300,500);
					break;
				case self::CITY_ID_HEFEI:
					$option = array(50,80,100,120,150,200,300);
					break;
				case self::CITY_ID_YANTAI:
					$option = array(40,60,80,100,150,200);
					break;
				default:
					$option = array(30, 40, 50, 60, 80, 100, 150, 200);
			}
		}

		if($constType == self::CONST_TYPE_RENT) {
			switch($cityId) {
				case self::CITY_ID_BEIJING:
					$option = array(1500,2500,3500,4500,5500,6500);
					break;
				case self::CITY_ID_SHANGHAI:
					$option = array(1000,2000,3000,5000,8000,10000);
					break;
				case self::CITY_ID_GUANGZHOU:
					$option = array(500,1000,1500,2000,3000,4000,5000);
					break;
				case self::CITY_ID_SHENZHEN:
					$option = array(500,1000,2000,3000,5000,8000,10000);
					break;
				case self::CITY_ID_TIANJIN:
					$option = array(500,800,1500,2000,3000,5000);
					break;
				case self::CITY_ID_CHENGDU:
					$option = array(500,1000,2000,3000,5000,8000);
					break;
				case self::CITY_ID_NANJING:
					$option = array(1000,1500,2000,3000);
					break;
				case self::CITY_ID_HANGZHOU:
					$option = array(1000,2000,3000,5000);
					break;
				case self::CITY_ID_QINGDAO:
					$option = array(1000,2000,3000,5000);
					break;
				case self::CITY_ID_DALIAN:
					$option = array(500,800,1500,2000,3000,5000);
					break;
				case self::CITY_ID_XIAMEN:
					$option = array(1000,1500,2000,3000,5000);
					break;
				case self::CITY_ID_WUHAN:
					$option = array(1000,1500,2000,2500,3000,4000,5000);
					break;
				case self::CITY_ID_CHONGQING:
					$option = array(600,1000,2000,3000,5000,8000);
					break;
				case self::CITY_ID_CHANGSHA:
					$option = array(600,1000,1500,2000,2500,3000,5000);
					break;
				case self::CITY_ID_JINAN:
					$option = array(600,1000,1500,2000,3000,5000);
					break;
				case self::CITY_ID_FOSHAN:
					$option = array(1000,1500,2000,3000,4000,5000);
					break;
				case self::CITY_ID_DONGGUAN:
					$option = array(500,1000,1500,2000,3000,4000,5000);
					break;
				case self::CITY_ID_HEFEI:
					$option = array(500,1000,1500,2000,3000,4500);
					break;
				case self::CITY_ID_YANTAI:
					$option = array(1000,1500,2000,3000);
					break;
				default:
					$option = array(600, 1000, 1500, 2000, 2500, 3000, 5000);
			}
		}

		if($constType == self::CONST_TYPE_RENT_AREA) {
			switch($cityId) {
				case self::CITY_ID_BEIJING:
					$option = array(30,50,70,90,120,140,160,200);
					break;
				case self::CITY_ID_GUANGZHOU:
					$option = array(40,60,80,100,120,144);
					break;
				case self::CITY_ID_SHENZHEN:
					$option = array(50,70,90,110,140,170,200);
					break;
				case self::CITY_ID_HANGZHOU:
					$option = array(50,70,90,120,140,160,200);
					break;
				case self::CITY_ID_WUHAN:
					$option = array(60,90,120,140,200,300);
					break;
				case self::CITY_ID_CHONGQING:
					$option = array(30,50,70,90,120,150,200,300);
					break;
				case self::CITY_ID_CHANGSHA:
					$option = array(50,70,90,110,130,150,200,300);
					break;
				case self::CITY_ID_JINAN:
					$option = array(50,70,90,120,144,200,300);
					break;
				case self::CITY_ID_FOSHAN:
					$option = array(40,60,80,100,120,160);
					break;
				case self::CITY_ID_DONGGUAN:
					$option = array(40,60,80,100,120,160,200);
					break;
				case self::CITY_ID_HEFEI:
					$option = array(40,60,80,100,120,160);
					break;
				case self::CITY_ID_YANTAI:
					$option = array(50,70,90,120,150,200,300);
					break;
				default:
					$option = array(50, 70, 90, 110, 130, 150, 200);
			}
		}

		if($constType == self::CONST_TYPE_SOURCE) {
			switch($cityId) {
				case self::CITY_ID_BEIJING:
					$option = array(104,500);
					break;
				case self::CITY_ID_SHANGHAI:
					$option = array(200);
					break;
				case self::CITY_ID_GUANGZHOU:
					$option = array(400);
					break;
				case self::CITY_ID_SHENZHEN:
					$option = array(104,500);
					break;
				case self::CITY_ID_TIANJIN:
					$option = array(104);
					break;
				case self::CITY_ID_CHENGDU:
					$option = array(100);
					break;
				case self::CITY_ID_NANJING:
					$option = array(104);
					break;
				case self::CITY_ID_HANGZHOU:
					$option = array(104);
					break;
				case self::CITY_ID_QINGDAO:
					$option = array(104);
					break;
				case self::CITY_ID_DALIAN:
					$option = array(104);
					break;
				case self::CITY_ID_XIAMEN:
					$option = array(100);
					break;
				case self::CITY_ID_WUHAN:
					$option = array(104);
					break;
				case self::CITY_ID_CHONGQING:
					$option = array(100);
					break;
				case self::CITY_ID_CHANGSHA:
					$option = array(104);
					break;
				case self::CITY_ID_JINAN:
					$option = array(104);
					break;
				case self::CITY_ID_FOSHAN:
					$option = array(700);
					break;
				case self::CITY_ID_DONGGUAN:
					$option = array(800);
					break;
				case self::CITY_ID_HEFEI:
					$option = array(104);
					break;
				case self::CITY_ID_YANTAI:
					$option = array(104);
					break;
				default:
					$option = array(100);
			}
		}

		if($constType == self::CONST_TYPE_RESBLOCK_PRICE) {
			switch($cityId) {
				case self::CITY_ID_BEIJING:
					$option = array(2,3,4,5,6);
					break;
				case self::CITY_ID_GUANGZHOU:
					$option = array(1,1.5,2,2.5,3);
					break;
				case self::CITY_ID_SHENZHEN:
					$option = array(2,3,4,5,6);
					break;
				case self::CITY_ID_TIANJIN:
					$option = array(1,1.5,2,2.5,3);
					break;
				case self::CITY_ID_CHENGDU:
					$option = array(0.5,0.8,1,1.5,2);
					break;
				case self::CITY_ID_NANJING:
					$option = array(1,1.5,2,2.5,3);
					break;
				case self::CITY_ID_HANGZHOU:
					$option = array(1,1.5,2,2.5,3);
					break;
				case self::CITY_ID_QINGDAO:
					$option = array(0.8,1,1.5,2,2.5);
					break;
				case self::CITY_ID_DALIAN:
					$option = array(0.8,1,1.5,2,2.5);
					break;
				case self::CITY_ID_XIAMEN:
					$option = array(2,2.5,3,3.5,4);
					break;
				case self::CITY_ID_WUHAN:
					$option = array(0.8,1,1.5,2,2.5);
					break;
				case self::CITY_ID_CHONGQING:
					$option = array(0.5,0.8,1,1.5);
					break;
				case self::CITY_ID_CHANGSHA:
					$option = array(0.5,0.8,1,1.5);
					break;
				case self::CITY_ID_JINAN:
					$option = array(0.5,0.8,1,1.5,2);
					break;
				case self::CITY_ID_FOSHAN:
					$option = array(0.5,0.8,1,1.5,2);
					break;
				case self::CITY_ID_DONGGUAN:
					$option = array(0.5,0.8,1,1.5,2);
					break;
				case self::CITY_ID_HEFEI:
					$option = array(0.7,1,1.5,1.7,2,2.5);
					break;
				case self::CITY_ID_YANTAI:
					$option = array(0.7,0.9,1.1,1.3,1.5);
					break;
				default:
					$option = array(1, 2, 3, 4, 5);
			}
		}

		if($constType == self::CONST_TYPE_TAG) {
			switch($cityId) {
				case self::CITY_ID_SHANGHAI:
					$option = array(全部标签,新上,有钥匙);
					break;
				default:
					$option = array("全部标签", "独家", "新上", 4 => "有钥匙");
			}
		}

		if($constType == self::CONST_TYPE_AREA) {
			switch($cityId) {
				case self::CITY_ID_GUANGZHOU:
					$option = array(40,60,80,100,120,144);
					break;
				case self::CITY_ID_SHENZHEN:
					$option = array(50,70,90,110,140,170,200);
					break;
				case self::CITY_ID_NANJING:
					$option = array(60,90,110,130,150,200);
					break;
				case self::CITY_ID_HANGZHOU:
					$option = array(50,70,90,120,140,160,200);
					break;
				case self::CITY_ID_WUHAN:
					$option = array(60,90,120,140,200,300);
					break;
				case self::CITY_ID_CHONGQING:
					$option = array(30,50,70,90,120,150,200,300);
					break;
				case self::CITY_ID_CHANGSHA:
					$option = array(50,70,90,110,130,150,200,300);
					break;
				case self::CITY_ID_JINAN:
					$option = array(50,70,90,120,144,200,300);
					break;
				case self::CITY_ID_FOSHAN:
					$option = array(40,60,80,100,120,160);
					break;
				case self::CITY_ID_DONGGUAN:
					$option = array(40,60,80,100,120,144,200);
					break;
				case self::CITY_ID_HEFEI:
					$option = array(50,70,90,120,150,200,300);
					break;
				case self::CITY_ID_YANTAI:
					$option = array(50,70,90,120,150,200,300);
					break;
				default:
					$option = array(50, 70, 90, 110, 130, 150, 200);
			}
		}

		if($constType == self::CONST_TYPE_ROOM) {
			switch($cityId) {
				case self::CITY_ID_CHENGDU:
					$option = array("不限","一室","二室","三室","三室以上");
					break;
				case self::CITY_ID_NANJING:
					$option = array("不限","一室","二室","三室","三室以上");
					break;
				case self::CITY_ID_HANGZHOU:
					$option = array("不限","一室","二室","三室","三室以上");
					break;
				case self::CITY_ID_QINGDAO:
					$option = array("不限","一室","二室","三室","四室","四室以上");
					break;
				case self::CITY_ID_JINAN:
					$option = array("不限","一室","二室","三室","四室","四室以上");
					break;
				default:
					$option = array("不限", "一室", "二室", "三室", "四室", "五室", "五室以上");
			}
		}

        return $option;
    }

}