<?php

interface ChannelInterface{
    /**
     *定义所有频道
     */
	const CHANNEL_ERSHOUFANG = 'ershoufang';
	const CHANNEL_DITIEFANG = 'ditiefang';
	const CHANNEL_ZUFANG = 'zufang';
	const CHANNEL_DITIEZUFANG = 'ditiezufang';
	const CHANNEL_SCHOOL = 'school';
	const CHANNEL_SCHOOL_V2 = 'school_v2';
	const CHANNEL_RESBLOCK = 'resblock';
	const CHANNEL_RESBLOCK_V2 = 'resblock_v2';
	const CHANNEL_FRAME = 'frame';
	const CHANNEL_XINFANG = 'xinfang';
	const CHANNEL_XINFANG_APP = 'xinfang_app';
	const CHANNEL_CHENGJIAO = 'chengjiao';
	const CHANNEL_FANGJIA = 'fangjia';
	const CHANNEL_DITU = 'ditu';
	const CHANNEL_BAIKE = 'baike';
	const CHANNEL_REDIAN = 'redian';
	const CHANNEL_REDIANPC = 'redianpc';
	const CHANNEL_JINGJIREN = 'jingjiren';
	const CHANNEL_WENDA = 'wenda';
	const CHANNEL_CARD = 'card';
	const CHANNEL_SCHOOL_CARD = 'school_card';
	const CHANNEL_SEARCH = 'search';
	const CHANNEL_ZIRU = 'ziru';
	const CHANNEL_LICAI = 'licai';
	const CHANNEL_MAIFANG = 'maifang';
	const CHANNEL_ZIXUN = 'zixun';
	const CHANNEL_MAIFANGGONGLUE = 'maifanggonglue';
	const CHANNEL_XUEQU_ZHAOFANG = 'xuequzhaofang';
	const CHANNEL_TOOLS = 'tools';
	const CHANNEL_LVJU = 'lvju';
	const CHANNEL_HOT_V2 = 'hot_v2';
	const CHANNEL_HOT = 'hot';
	const CHANNEL_OVERSEA = 'oversea';
}