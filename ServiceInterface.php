<?php

interface ServiceInterface{
    /**
     *定义所有服务
     */
	const SERVICE_TOUSU = 'tousu';
	const SERVICE_BAODAN = 'baodan';
	const SERVICE_TRUEHOUSE = 'zhenfangyuan';
	const SERVICE_LOUPAN_NUM = 'loupan_number';
	const SERVICE_HEATING_TYPE = 'heating_type';
	const SERVICE_PURCHASE_RESTRICTION = 'purchase_restriction';
	const SERVICE_TAX_FREE = 'tax_free';
	const SERVICE_REACH_2YEAR = 'reach_2year';
	const SERVICE_SUBWAY = 'subway_house';
	const SERVICE_SCHOOL = 'school_house';
	const SERVICE_MIDDLESCHOOL = 'middleschool_house';
	const SERVICE_HOUSE_TYPE = 'house_type';
	const SERVICE_BUILDING_YEAR = 'building_year';
	const SERVICE_QUICK_ACT = 'quick_acting';
	const SERVICE_SE_SOURCE = 'se_source';
	const SERVICE_DAIKAN = 'daikan';
	const SERVICE_LIST_V2 = 'list_v2';
	const SERVICE_LIST_HIGHLIGHT = 'list_highlight';
	const SERVICE_FRAME_STRUCTURE = 'frame_structure';
	const SERVICE_HAS_ELEVATOR = 'has_elevator';
	const SERVICE_AGENT_CHECK_FILTER = 'agent_check_filter';
	const SERVICE_AGENT_LAW_FILTER = 'agent_law_filter';
	const SERVICE_RESBLOCK_TYPE_FILTER = 'resblock_type_filter';
	const SERVICE_ZIROOM_ONLY = 'ziroom_only';
	const SERVICE_BAND = 'ziroom_band';
	const SERVICE_RENT_EQUIPMENT = 'rent_equipment';
	const SERVICE_NEW_AGENT_PHONE = 'new_agent_phone';
	const SERVICE_NEW_AGENT_PHONE_APP = 'new_agent_phone_app';
	const SERVICE_RESBLOCK_CHENGJIAOORDER_FILTER = 'resblock_chengjiao_order_filter';
	const SERVICE_AGENT_DAIKAN_COMMENT = 'agent_daikan_comment';
	const SERVICE_RESBLOCK_WENDA = 'resblock_wenda';
	const SERVICE_YEZHU_MAIFANG = 'yezhu_maifang';
	const SERVICE_YEZHU_GUJIA = 'yezhu_gujia';
	const SERVICE_YEZHU_CHUZU = 'yezhu_chuzu';
	const SERVICE_LOGISTICS = 'logistics';
	const SERVICE_YUEDAIKAN = 'link_yuedaikan';
	const SERVICE_BDA_DATA = 'bda_data';
	const SERVICE_CITY_HOT = 'city_hot';
	const SERVICE_SALE_HELPER = 'sale_helper';
	const SERVICE_RESBLOCK_DETAIL_V2 = 'resblock_detail_v2';
}